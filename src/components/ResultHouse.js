import React from 'react';
import './ResultHouse.css';
import GetHouseInfo from "../api/GetHouseInfo";
import Modal from "react-responsive-modal";
import HouseFullInfoContent from "./HouseFullInfoContent";
class ResultHouse extends React.Component {
    constructor(props){
        super(props);
        this.state = {houseFullInfoModal: false};

        this.openHouseFullInfoModal = this.openHouseFullInfoModal.bind(this);
        this.closeHouseFullInfoModal = this.closeHouseFullInfoModal.bind(this);
    }


    openHouseFullInfoModal(){
        this.setState({houseFullInfoModal:true});
    }
    closeHouseFullInfoModal(){
        this.setState({houseFullInfoModal:false});
    }

    render()
    {
        let deal_type;
        let building_loc;

        if (this.props.info.dealType === "sell") {
            deal_type = "فروش";
            building_loc = "sell-loc"
        } else {
            deal_type = "رهن و اجاره";
            building_loc = "rent-loc";
        }
        const { houseFullInfoModal } = this.state;
        return (
            <div className="col-xs-12 col-sm-6 bsrtl ">
                <Modal open={houseFullInfoModal} onClose={this.closeHouseFullInfoModal} center>
                    <HouseFullInfoContent house_info={this.props.house_info} set_house_info={this.props.set_house_info}/>
                </Modal>
                {/*data-toggle="modal" data-target="#house-full-info-modal"*/}
                <div className="nopadding marginStyle houseInfo"
                    onClick={
                        () => {
                            this.openHouseFullInfoModal();
                            GetHouseInfo(this.props.info.id)
                                .then((response) => {
                                    this.props.set_house_info(response)
                                })
                        }}>
                    <div id="sell-rent-box">
                        <div className={this.props.info.dealType + " sell-rent"}>
                            {deal_type}
                        </div>
                    </div>
                    <img src={this.props.info.imageURL}/>
                    <div className="properties">
                        <div className="house-details">
                            {this.props.info.area}
                            متر‌مربع
                        </div>
                        <div className="house-details">
                            <i className={["fa fa-map-marker", building_loc].join(' ')}/> {this.props.info.address}
                        </div>
                        <div className="straight-line"/>
                        <div className="house-details">
                            قیمت {this.props.info.dealType === "rent" ? this.props.info.basePrice : this.props.info.sellPrice}
                            <span className="grey-text"> تومان</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ResultHouse;