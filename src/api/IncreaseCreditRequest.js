import React from 'react';

function increaseCreditRequest (amount) {
    return fetch('/api/IncreaseCredit?balance=' + amount, {
            headers: {
                'jwt': window.localStorage.getItem('jwt')
            }
        }
    ).then(response => {
        if (response.status === 200)
            return response.json();
        else {
            if (response.status === 401) window.localStorage.removeItem('jwt');
            throw response;
        }
    }).catch(response => {
        alert("request to server (/ajaxIncreaseCredit) failed. status:" + response.status);
        return {success: "NotOk", response: response}
    });
}

export default increaseCreditRequest;