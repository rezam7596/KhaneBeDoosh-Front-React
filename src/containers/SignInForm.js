import React from 'react';
import './SignInForm.css';
import GetJWT from "../api/GetJWT";

class SignInForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {username:'', password:'', usernameWarning:false};

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleInputChange(event) {
        if(event.target.name === "username"
            && event.target.value.match(/^[A-Za-z][A-Za-z0-9_]*$/) == null) {

            this.setState({usernameWarning:true});
            return;
        }

        this.setState({
            [event.target.name]: event.target.value,
            usernameWarning:false
        });
    }

    handleSubmit() {
        GetJWT(this.state.username, this.state.password).then(
            (response)=> {
                if(response.success){
                    window.localStorage.setItem('jwt', response.jwt);
                    alert("logged in successfully");
                    //close sign-in modal if needed
                    if(typeof this.props.onCloseSignInModal !== 'undefined')
                        this.props.onCloseSignInModal();
                } else
                    alert("log in failed");
            }
        ).catch(()=>{alert("connection failed")})
    }

    render() {
        return (
            <div id="sign-in-form">
                <form className="row no-padding">
                    <div>
                        <div className="col-xs-12 bsrtl">
                            <input type="text" name="username" placeholder="نام کاربری" value={this.state.username} onChange={this.handleInputChange}/>
                            {this.state.usernameWarning ?
                                <ul className="warning">
                                    <li>نام کاربری فقط می‌تواند شامل حروف انگلیسی، اعداد و '_' باشد</li>
                                    <li>نام کاربری با 'ـ' نمی‌تواند شروع شود</li>
                                </ul> :
                                <div/>
                            }
                        </div>
                        <div className="col-xs-12 bsrtl">
                            <input type="password" name="password" placeholder="پسورد" value={this.state.password} onChange={this.handleInputChange}/>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-5 bsrtl">
                        <button type="button" onClick={this.handleSubmit}>ورود</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default SignInForm;