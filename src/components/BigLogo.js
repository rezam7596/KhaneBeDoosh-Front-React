import React from 'react';
import './BigLogo.css';

function BigLogo(props) {
	return (
		<section className="row" id="big-logo">
			<img src={require("./images/logo.png")}/>
			<div>خانه به دوش</div>
		</section>
	);
}

export default BigLogo;