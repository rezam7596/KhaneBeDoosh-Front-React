import React from 'react';
import './AddHouseButton.css'

function AddHouseButton(props) {
	return (
		<section id="add-house">
			<div data-toggle="modal" data-target="#add-new-house-modal">
				<span>صاحب خانه هستید؟ خانه خود را ثبت کنید</span>
			</div>
		</section>
	);
}

export default AddHouseButton;