import React from 'react';
import './UserDropDownMenu.css';
import getUserInfo from "../api/GetUserInfo";
import SignInForm from "./SignInForm";

/*
 * the default display of dropdown menu (id="user-drop-down-menu") is none.
 * you have to change display in some condition if you want to make it appear
 */
class UserDropDownMenu extends React.Component {
	constructor(props){
		super(props);
		this.state = {userInfo:{}};
		
		this.updateUserInfo = this.updateUserInfo.bind(this);
	}
	
	componentDidMount(){
		this.updateUserInfoInterval = setInterval(this.updateUserInfo, 2000);
	}
	
	componentWillUnmount(){
		clearInterval(this.updateUserInfoInterval);
	}
	
	updateUserInfo(){
		getUserInfo().then(newUserInfo => {
			this.setState({userInfo: newUserInfo});
        });
	}
	
	render() {
		return (
            <div id="user-drop-down-menu">
                <div id="dummy-div"/>
				{(window.localStorage.getItem('jwt') == null) ?
					<SignInForm/>
					:
					<div>
						<div>{this.state.userInfo.username}</div>
						<div id="credit">
							<span>اعتبار</span>
							<span>{this.state.userInfo.balance} تومان</span>
						</div>
						<div className="button-input">
							<input type="button" value="افزایش اعتبار" data-toggle="modal"
								   data-target="#increase-credit-modal"/>
						</div>
					   {/*<button type="button" onClick={window.localStorage.removeItem('jwt')}>*/}
						   {/*خروج*/}
						   {/*<i className="fa fa-sign-out" aria-hidden="true"/>*/}
					   {/*</button>*/}'
					</div>
				}
			</div>
		);
	}
}

export default UserDropDownMenu;