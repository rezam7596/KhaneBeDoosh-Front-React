import React from 'react';

function GetPhoneNumber (houseId) {
    return fetch('/api/GetPhoneNumber?houseId=' + houseId, {
            headers: {
                'jwt': window.localStorage.getItem('jwt')
            }
        }
    ).then(response => {
        if (response.status === 200)
            return response.json();
        else {
            if (response.status === 401) window.localStorage.removeItem('jwt');
            throw response;
        }
    }).catch(response => {
        alert("request to server (/ajaxGetPhoneNumber) failed. status:" + response.status);
        return {success:false, status: response.status, text: response}
    });
}

export default GetPhoneNumber;