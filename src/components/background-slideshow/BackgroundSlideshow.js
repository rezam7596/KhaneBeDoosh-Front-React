import React from 'react';
import './BackgroundSlideshow.css'

function BackgroundSlideshow(props) {
	return (
		<div id="slideshow" className="col-xs-12 col-md-12 no-padding">
			<div class="slide-wrapper">
				<img src={require("./images/casey-horner.jpg")} id="first-slide" className="slide"/>
				<img src={require("./images/luke-van-zyl.jpg")} id="second-slide" className="slide"/>
				<img src={require("./images/mahdiar-mahmoodi.jpg")} id="third-slide" className="slide"/>
				<img src={require("./images/michal-kubalczyk.jpg")} id="forth-slide" className="slide"/>
			</div>
		</div>
	);
}

export default BackgroundSlideshow;