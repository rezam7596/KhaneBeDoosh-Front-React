import React from 'react';
import './HouseFullInfoModal.css'
import IncreaseCreditInput from "../containers/IncreaseCreditForm";
import HouseFullInfoContent from "./HouseFullInfoContent";

function HouseFullInfoModal(props) {
	var incrementValue = 0;
	return (
		<div>
			<div className="modal fade" id="house-full-info-modal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div className="modal-dialog" role="document">
					<div className="modal-content">
						<div className="modal-header">
							<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 className="modal-title" id="myModalLabel">مشخصات کامل ملک</h4>
						</div>
						<div className="modal-body">
							<HouseFullInfoContent house_info={props.house_info} set_house_info={props.set_house_info}/>
						</div>
						<div className="modal-footer">
							<button type="button" className="btn btn-default" data-dismiss="modal">انصراف</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default HouseFullInfoModal;