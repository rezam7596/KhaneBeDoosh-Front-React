import React from 'react';
import './IncreaseCreditForm.css';
import increaseCreditRequest from "../api/IncreaseCreditRequest";

class IncreaseCreditForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {value: '', warning: ''};
		
		this.handleChange = this.handleChange.bind(this);
	}
	
	handleChange(event) {
		if(!isNaN(event.target.value)) {
			this.setState({value: event.target.value, warning:''});
		}else{
			this.setState({warning:'لطفا اعداد انگلیسی وارد کنید'});
		}
	}
	
	render() {
		return (
			<form id="increase-credit-form">
				<div id="input-area">
					<label htmlFor="increase-credit-value">
						تومان
					</label>
					<input type="text" id="increase-credit-value" value={this.state.value} onChange={this.handleChange} placeholder="مبلغ مورد نظر"/>
				</div>
				<button type="button" className="m-button" onClick={
					() => {
						increaseCreditRequest(this.state.value)
							.then(response => alert(JSON.stringify(response)))
                    }
				}>
					افزایش اعتبار
				</button>
				<div id="warning">
					{this.state.warning}
				</div>
			</form>
		);
	}
}

export default IncreaseCreditForm;