import React from 'react';
import './Footer.css';

function Footer(props) {
	return (
		<footer>
			<div className="footer-text">تمامی حقوق مادی و معنوی این سایت متعلق به رضا محمدی و فریما فتاحی بیات می‌باشد.</div>
			<div className="social-contacts">
				<div>
					<img src={require("./images/Instagram_logo.png")}/>
				</div>
				<div>
					<img src={require("./images/Twitter_logo.png")}/>
				</div>
				<div>
					<img src={require("./images/Telegram_logo.png")}/>
				</div>
			</div>
		</footer>
	);
}

export default Footer;