import * as $ from 'jquery';
import React from 'react';
import './HouseFullInfoContent.css'
import GetPhoneNumber from "../api/GetPhoneNumber";
import GetHouseInfo from "../api/GetHouseInfo";
import Modal from "react-responsive-modal";
import SignInForm from "../containers/SignInForm";

class HouseFullInfoContent extends React.Component {
	constructor(props){
		super(props);
		this.state = {signInModal:false};

		this.openSignInModal = this.openSignInModal.bind(this);
		this.closeSignInModal = this.closeSignInModal.bind(this);
	}

    openSignInModal(){
        this.setState({signInModal:true});
    }
    closeSignInModal(){
        this.setState({signInModal:false});
    }

    render() {
        const price = (this.props.house_info.dealType === "rent") ?
            (<div>
                <div>
                    رهن: {this.props.house_info.basePrice} تومان
                </div>
                <div>
                    اجاره: {this.props.house_info.rentPrice} تومان
                </div>
            </div>) :
            (<div>
                قیمت: {this.props.house_info.sellPrice} تومان
            </div>);
        const dealTypeContent = this.props.house_info.dealType === "rent" ? "رهن و اجاره" : "فروش";
        const buildingTypeContent = this.props.house_info.buildingType === "villa" ? "ویلایی" : "آپارتمانی";
        const getPhoneNumber = <div id="get-phone" onClick={
            () => GetPhoneNumber(this.props.house_info.id)
                .then(response => {
                    if (response.success) {
                        GetHouseInfo(this.props.house_info.id)
                            .then(response => this.props.set_house_info(response))
                        alert(JSON.stringify(response));
                    } else if(response.status === 401) {
                        this.openSignInModal();
                    }
                })
        }>
            مشاهده شماره مالک
        </div>;

        const {signInModal} = this.state;
        return (
            <div id="house-full-info-content">
                <Modal open={signInModal} onClose={this.closeSignInModal} center>
					<SignInForm onCloseSignInModal={this.closeSignInModal}/>
                </Modal>
                <div id="info-text" className="col-xs-12 col-sm-4 bsrtl">
                    <div>
                        <div id="deal-type-box">
                            {dealTypeContent}
                        </div>
                        <div>
                            شماره مالک: {this.props.house_info.phone}
                        </div>
                        <div>
                            نوع ساختمان: {buildingTypeContent}
                        </div>
                        <div>
                            {price}
                        </div>
                        <div>
                            آدرس: {this.props.house_info.address}
                        </div>
                        <div>
                            متراژ: {this.props.house_info.area} مترمربع
                        </div>
                        <div>
                            توضیحات: {this.props.house_info.description}
                        </div>
                    </div>
                </div>
                <div id="house-image" className="col-xs-12 col-sm-8 bsrtl">
                    <img src={this.props.house_info.imageURL}/>
                    {this.props.house_info.paid === false ? getPhoneNumber : <div/>}
                </div>

                <div/>
            </div>
        );
    }
}

export default HouseFullInfoContent;