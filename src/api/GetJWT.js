import React from 'react';

function GetJWT (username, password) {
    return fetch('/api/GetJWT?username=' + username + '&password=' + password)
        .then(response => {
            return response.json();
        })
        .catch(response => {
            console.log("request to server (/api/GetJWT) failed. status:" + response.status);
            return {success:false, status:response.status, response:response}
        });
}

export default GetJWT;