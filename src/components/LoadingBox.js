import React from 'react';
import ReactLoading from 'react-loading';
import './LoadingBox.css'
function LoadingBox(props) {
	return (
		props.loading === true ?
			(<div id="loading-box">
				<div>
					<ReactLoading type="spin" color="#1b6d85" height={300} width={200}/>
				</div>
			</div>) :
			<div></div>
	);
}

export default LoadingBox;