import React from 'react';
import './UserSpace.css'
import UserDropDownMenu from '../../../containers/UserDropDownMenu';
import GetUserInfo from "../../../api/GetUserInfo";

class UserSpace extends React.Component{
    constructor(props) {
        super(props);
        this.state = {loggedIn: false, context: 'ناحیه کاربری'};

        this.updateLoggedInState = this.updateLoggedInState.bind(this);
    }

    componentDidMount(){
        this.updateUserInfoInterval = setInterval(
        	this.updateLoggedInState,
			2000
		);
    }

    componentWillUnmount(){
        clearInterval(this.updateUserInfoInterval);
    }

    updateLoggedInState() {
        if(this.state.loggedIn !== (window.localStorage.getItem('jwt')!=null) ) {
            this.setState({loggedIn : window.localStorage.getItem('jwt')!=null});

            if (this.state.loggedIn)
                GetUserInfo().then(response => {
                    this.setState({context: response.username})
                });
            else
                this.setState({context: 'ورود'});
        }
	}

    render() {
        return (
            <div id="userSpace">
				<span>
					<i className="fa fa-user"></i> {this.state.context}
				</span>
                <UserDropDownMenu/>
            </div>
        );
    }
}

export default UserSpace;