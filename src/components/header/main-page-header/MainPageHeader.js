import React from 'react';
import './MainPageHeader.css';
import UserSpace from './UserSpace';
import IncreaseCreditModal from "../../IncreaseCreditModal";

function MainPageHeader(props) {
	return (
		<header id="main-page-header">
			<UserSpace/>
		</header>
	);
}

export default MainPageHeader;