import React from 'react';

function GetUserInfo () {
    return fetch('/api/GetUserInfo', {
            headers: {
                'jwt': window.localStorage.getItem('jwt')
            }
        }
    ).then(response => {
        if (response.status === 200)
            return response.json();
        else {
            if(response.status === 401) window.localStorage.removeItem('jwt');
            throw response;
        }
    }).catch(response => {
        console.log("request to server (/ajaxGetUserInfo) failed. status:" + response.status);
        return {success: "NotOk", response: response}
    });
}

export default GetUserInfo;