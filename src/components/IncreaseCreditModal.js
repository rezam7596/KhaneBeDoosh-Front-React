import React from 'react';
import './IncreaseCreditModal.css'
import IncreaseCreditInput from "../containers/IncreaseCreditForm";

function IncreaseCreditModal(props) {
	return (
		<div>
			<div className="modal fade" id="increase-credit-modal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div className="modal-dialog" role="document">
					<div className="modal-content">
						<div className="modal-header">
							<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 className="modal-title" id="myModalLabel">افزایش اعتبار</h4>
						</div>
						<div className="modal-body">
							<IncreaseCreditInput/>
						</div>
						<div className="modal-footer">
							<button type="button" className="btn btn-default" data-dismiss="modal">انصراف</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default IncreaseCreditModal;