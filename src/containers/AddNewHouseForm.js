import React from 'react';
import './AddNewHouseForm.css'
import AddNewHouse from '../api/AaddNewHouse'

class AddNewHouseForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {dealType:'', buildingType:'', area:'', address:'', basePrice:'', rentPrice:'', sellPrice:'', phone:'', description:'', warning: ''};
		
		this.handleDealTypeChange = this.handleDealTypeChange.bind(this);
		this.handleBuildingTypeChange = this.handleBuildingTypeChange.bind(this);
		this.handleAreaChange = this.handleAreaChange.bind(this);
		this.handleAddressChange = this.handleAddressChange.bind(this);
		this.handleBasePriceChange = this.handleBasePriceChange.bind(this);
		this.handleRentPriceChange = this.handleRentPriceChange.bind(this);
		this.handleSellPriceChange = this.handleSellPriceChange.bind(this);
		this.handlePhoneChange = this.handlePhoneChange.bind(this);
		this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
	}
	
	handleDealTypeChange(event){
		this.setState({dealType: event.target.value});
	}
	handleBuildingTypeChange(event) {
		this.setState({buildingType: event.target.value, warning:''});
	}
	handleAreaChange(event) {
		if(!isNaN(event.target.value))
			this.setState({area: event.target.value, warning:''});
		else
			this.setState({warning:'لطفا اعداد انگلیسی وارد کنید'});
	}
	handleAddressChange(event) {
		this.setState({address: event.target.value, warning:''});
	}
	handleBasePriceChange(event) {
		if(!isNaN(event.target.value))
			this.setState({basePrice: event.target.value, warning:''});
		else
			this.setState({warning:'لطفا اعداد انگلیسی وارد کنید'});
	}
	handleRentPriceChange(event) {
		if(!isNaN(event.target.value))
			this.setState({rentPrice: event.target.value, warning:''});
		else
			this.setState({warning:'لطفا اعداد انگلیسی وارد کنید'});
	}
	handleSellPriceChange(event) {
		if(!isNaN(event.target.value))
			this.setState({sellPrice: event.target.value, warning:''});
		else
			this.setState({warning:'لطفا اعداد انگلیسی وارد کنید'});
	}
	handlePhoneChange(event) {
		//todo: check length
		if(!isNaN(event.target.value))
			this.setState({phone: event.target.value, warning:''});
		else
			this.setState({warning:'لطفا اعداد انگلیسی وارد کنید'});
	}
	handleDescriptionChange(event) {
		this.setState({description: event.target.value, warning:''});
	}
	
	render() {
		const priceInputs = (this.state.dealType === "rent") ?
			(
				<div className="col-xs-12 bsrtl no-margin no-padding">
					<div className="input-area col-xs-12 col-sm-6 bsrtl">
						<label htmlFor="base-price">
							تومان
						</label>
						<input type="text" id="base-price" value={this.state.basePrice} onChange={this.handleBasePriceChange} placeholder="قیمت رهن"/>
					</div>
					<div className="input-area col-xs-12 col-sm-6 bsrtl">
						<label htmlFor="rent-price">
							تومان
						</label>
						<input type="text" id="rent-price" value={this.state.rentPrice} onChange={this.handleRentPriceChange} placeholder="قیمت اجاره"/>
					</div>
				</div>
			): (this.state.dealType === "sell") ?
			(
				<div className="input-area col-xs-12 col-sm-6 bsrtl">
					<label htmlFor="sell-price">
						تومان
					</label>
					<input type="text" id="sell-price" value={this.state.sellPrice} onChange={this.handleSellPriceChange} placeholder="قیمت فروش"/>
				</div>
			):
			(
				<div/>
			);
		
		return (
			<form id="add-new-house-form" className="row">
				<div className="col-xs-12">
					<div className="radio-input">
						<input type="radio" name="dealType" id="dealTypeOpt1" value="rent" onChange={this.handleDealTypeChange} checked={this.state.dealType === "rent"}/>
						<label htmlFor="dealTypeOpt1">رهن و اجاره</label>
					</div>
					<div className="radio-input">
						<input type="radio" name="dealType" id="dealTypeOpt2" value="sell" onChange={this.handleDealTypeChange} checked={this.state.dealType === "sell"}/>
						<label htmlFor="dealTypeOpt2">فروش</label>
					</div>
				</div>
				<div id="warning">
					{this.state.warning}
				</div>
				<div className="input-area col-xs-12 col-sm-6 bsrtl">
					<label className="vis-hidden text-label" htmlFor="building-type">hidden label</label>
					<select id="building-type" onChange={this.handleBuildingTypeChange}>
						<option value="" disabled selected>نوع ملک</option>
						<option value="apartment">آپارتمانی</option>
						<option value="villa">ویلایی</option>
					</select>
				</div>
				<div className="input-area col-xs-12 col-sm-6 bsrtl">
					<label htmlFor="area">
						متر مربع
					</label>
					<input type="text" id="area" value={this.state.area} onChange={this.handleAreaChange} placeholder="متراژ"/>
				</div>
				<div className="input-area col-xs-12 col-sm-6 bsrtl">
					<label className="vis-hidden text-label" htmlFor="building-type">hidden label</label>
					<input type="text" id="address" value={this.state.address} onChange={this.handleAddressChange} placeholder="آدرس"/>
				</div>
				<div className="input-area col-xs-12 col-sm-6 bsrtl">
					<label className="vis-hidden text-label" htmlFor="building-type">hidden label</label>
					<input type="text" id="phone" value={this.state.phone} onChange={this.handlePhoneChange} placeholder="شماره تماس"/>
				</div>
				{priceInputs}
				<div className="input-area col-xs-12">
					<label className="vis-hidden text-label" htmlFor="building-type">hidden label</label>
					<input type="text" id="description" value={this.state.description} onChange={this.handleDescriptionChange} placeholder="توضیحات"/>
				</div>
				<div className="col-xs-12 col-sm-6" >
					<button type="button" className="m-button bsrtl" onClick={() => {
						AddNewHouse(
							this.state.buildingType,
							this.state.area,
							this.state.dealType,
							this.state.basePrice,
							this.state.rentPrice,
							this.state.sellPrice,
							this.state.address,
							this.state.phone,
							this.state.description
						).then((response) => alert(JSON.stringify(response)))}}>
						ثبت ملک
					</button>
				</div>
			</form>
		);
	}
}

export default AddNewHouseForm;