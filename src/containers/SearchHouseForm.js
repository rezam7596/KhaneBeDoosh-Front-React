import React from 'react';
import { Link } from 'react-router-dom'
import './SearchHouseForm.css';
import SearchHouse from "../api/SearchHouse";

class SearchHouseForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {buildingType:'', maxPrice: '', maxArea:'', selectedOption:'', areaWarning:'', priceWarning:''};

        this.priceHandler = this.priceHandler.bind(this);
        this.areaHandler = this.areaHandler.bind(this);
        this.buildingTypeHandler = this.buildingTypeHandler.bind(this);
        this.optionChangeHandler = this.optionChangeHandler.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    buildingTypeHandler(event){
        this.setState({buildingType: event.target.value});
	}
    priceHandler(event){
        if(!isNaN(event.target.value)) {
            this.setState({maxPrice: event.target.value, priceWarning:''});
        }else{
            this.setState({priceWarning:'لطفا اعداد انگلیسی وارد کنید'});
        }
	}
	areaHandler(event){
        if(!isNaN(event.target.value)) {
            this.setState({maxArea: event.target.value, areaWarning:''});
        }else{
            this.setState({areaWarning:'لطفا اعداد انگلیسی وارد کنید'});
        }
	}
    optionChangeHandler(event){
        this.setState({selectedOption: event.target.value});
	}

    handleSubmit() {
        SearchHouse(this.state.buildingType, this.state.maxPrice, this.state.maxArea, this.state.selectedOption, this.props.set_loading).then(
			(response)=> {
				this.props.set_search_result(response);
				this.props.set_loading(false);
			}
		).catch(()=>{alert("connection failed")})
    }

    render() {
        return (
            <section id="search-house-form">
                <form className="row no-padding">
                    <div>
                        <div className="col-xs-12 col-sm-4 bsrtl">
                            <label className="vis-hidden text-label" htmlFor="buildingType">hidden label</label>
                            <select type="text" id="buildingType" value={this.state.buildingType} onChange={this.buildingTypeHandler}>
                                <option value="" disabled selected>نوع ملک</option>
                                <option value="apartment">آپارتمانی</option>
                                <option value="villa">ویلایی</option>
                            </select>
                        </div>
                        <div className="col-xs-12 col-sm-4 bsrtl">
                            <label className="text-label" htmlFor="maxPrice">تومان</label>
                            <input type="text" id="maxPrice" placeholder="حداکثر قیمت" value={this.state.maxPrice} onChange={this.priceHandler}/>
                            {this.state.priceWarning}
                        </div>
                        <div className="col-xs-12 col-sm-4 bsrtl">
                            <label className="text-label" htmlFor="maxArea">متر مربع</label>
                            <input type="text" id="maxArea" placeholder="حداقل متراژ" value={this.state.maxArea} onChange={this.areaHandler}/>
                            {this.state.areaWarning}
                        </div>
                    </div>
                    <div className="col-xs-5 col-sm-3 bsrtl radio-input">
                        <input type="radio" name="dealType" id="dealTypeOpt1" value="rent" onChange={this.optionChangeHandler}/>
                        <label htmlFor="dealTypeOpt1">رهن و اجاره</label>
                    </div>
                    <div className="col-xs-5 col-sm-3 col-sm-offset-1 bsrtl radio-input">
                        <input type="radio" name="dealType" id="dealTypeOpt2" value="sell" onChange={this.optionChangeHandler}/>
                        <label htmlFor="dealTypeOpt2">خرید</label>
                    </div>
                    <div className="col-xs-12 col-sm-5 bsrtl">
                        <Link to="/search" onClick={this.handleSubmit}><button type="button">جستجو</button></Link>
                    </div>
                </form>
            </section>
        );
    }
}

export default SearchHouseForm;