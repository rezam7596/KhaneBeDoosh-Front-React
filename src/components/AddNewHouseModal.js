import React from 'react';
import './AddNewHouseModal.css'
import AddNewHouseForm from "../containers/AddNewHouseForm";

function AddNewHouseModal(props) {
	return (
		<div>
			<div className="modal fade" id="add-new-house-modal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div className="modal-dialog" role="document">
					<div className="modal-content">
						<div className="modal-header">
							<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 className="modal-title" id="myModalLabel">ثبت ملک جدید در خانه به دوش</h4>
						</div>
						<div className="modal-body">
							<AddNewHouseForm/>
						</div>
						<div className="modal-footer">
							<button type="button" className="btn btn-default" data-dismiss="modal">انصراف</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default AddNewHouseModal;