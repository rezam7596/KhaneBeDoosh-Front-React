import React from 'react';
import Footer from "../components/Footer";
import CommonHeader from "../components/header/common-header/CommonHeader";
import TitleBar from "../components/TitleBar"
import ResultHouse from "../components/ResultHouse"
import './SearchPage.css'
import HouseFullInfoModal from "../components/HouseFullInfoModal";
import IncreaseCreditModal from "../components/IncreaseCreditModal";
import SignInModal from "../components/SignInModal";

class SearchPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {currentHouseInfo: {}, openHouseFullInfoModal: false};
		
		this.setCurrentHouseInfo = this.setCurrentHouseInfo.bind(this);
	}
	
	setCurrentHouseInfo(info){
	    this.setState({currentHouseInfo: info})
    }
	
    render() {
		const page_notice_content = this.props.search_result.length === 0 ?
			"هیچ خانه‌ای یافت نشد" : "برای مشاهده اطلاعات بیشتر درباره‌ی هر ملک روی آن کلیک کنید";
        return (
            <div>
                <IncreaseCreditModal/>
                <HouseFullInfoModal house_info={this.state.currentHouseInfo} set_house_info={this.setCurrentHouseInfo}/>
                <SignInModal/>
                <CommonHeader/>
                <TitleBar/>
                <div id="wrapper" className="row">
					<div id="page-notice" className="col-xs-12">
                        {page_notice_content}
					</div>
                    <div className="col-xs-12 col-sm-10 col-sm-offset-1 row" >
                    {this.props.search_result.map((item, index)=> (
                        <ResultHouse info={item} set_house_info={this.setCurrentHouseInfo} house_info={this.state.currentHouseInfo}/>
                    ))}
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default SearchPage;