import React from 'react';
import './CommonHeader.css';
import CommonHeaderLogo from './CommonHeaderLogo';
import UserSpace from './UserSpace';

function CommonHeader(props) {
	return (
		<header className="navbar-fixed-top" id="common-header">
			<div>
				<CommonHeaderLogo/>
				<UserSpace/>
			</div>
		</header>
	);
}

export default CommonHeader;