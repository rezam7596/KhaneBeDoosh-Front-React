import React from 'react';

function GetHouseInfo (houseId) {
    return fetch('/api/HouseInfo?houseId=' + houseId, {
        headers: {
            'jwt': window.localStorage.getItem('jwt')
        }
    }).then(response => {
        if (response.status === 200)
            return response.json();
        else
            throw response;
    }).catch(response => {
        alert("request to server (/ajaxHouseInfo) failed. status:" + response.status);
        return {success: "NotOk", response: response}
    });

}
export default GetHouseInfo;