import React from 'react';
import {Link} from 'react-router-dom';
import './CommonHeaderLogo.css';

function CommonHeaderLogo (props) {
	return (
		<Link to="/" class="react-link">
			<div id="header-logo">
				<img src={require('./logo.png')}/> خانه به دوش
			</div>
		</Link>
	);
}

export default CommonHeaderLogo;