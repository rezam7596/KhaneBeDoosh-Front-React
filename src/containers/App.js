import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ReactLoading from 'react-loading';
import MainPage from "./MainPage";
import SearchPage from "./SearchPage"
import LoadingBox from "../components/LoadingBox";

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {currentPage: "MainPage", searchResult: [], loading: false};
		
		this.setCurrentPage = this.setCurrentPage.bind(this);
		this.setLoading = this.setLoading.bind(this);
	}
	
	setCurrentPage(page) {
		this.setState({currentPage: page});
	}
	
	setLoading(isLoading) {
		this.setState({loading: isLoading});
	}
	
	render() {
		const Example = ({type, color}) => (
			<ReactLoading type={type} color={color} height='667' width='375'/>
		);
		return (
			<div>
				<LoadingBox loading={this.state.loading}/>
				<Switch>
					<Route path='/' exact render={() => (<MainPage  set_search_result={result => this.setState({searchResult: result})}
																	set_loading={this.setLoading}/>)}/>
					<Route path='/search' render={() => (<SearchPage search_result={this.state.searchResult}
																	set_loading={this.setLoading}/>)}/>
					<Route render={() => <h1>چنین صفحه‌ای یافت نشد</h1>}/>
				</Switch>
			</div>
		)
	}
}

export default App;