import React from 'react';

function AddNewHouse (buildingType, area, dealType, basePrice, rentPrice, sellPrice, address, phone, description) {
    return fetch('/api/AddNewHouse?' +
        'building_type=' + buildingType +
        '&area=' + area +
        '&deal_type=' + dealType +
        '&base_price=' + basePrice +
        '&rent_price=' + rentPrice +
        '&sell_price=' + sellPrice +
        '&address=' + address +
        '&phone=' + phone +
        '&description=' + description, {
            headers: {
                'jwt': window.localStorage.getItem('jwt')
            }
        }
    ).then(response => {
        if (response.status === 200)
            return response.json();
        else {
            if(response.status === 401) window.localStorage.removeItem('jwt');
            throw response;
        }
    }).catch(response => {
		alert("request to server (/ajaxAddNewHouse) failed. status:" + response.status);
		return {success: "NotOk", response: response}
	});
}

export default AddNewHouse;