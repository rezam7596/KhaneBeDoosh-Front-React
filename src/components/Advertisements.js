import React from 'react';
import './Advertisements.css'

function Advertisements(props) {
	return (
		<section id="ads">
			<div>
				<div id="first-ad" class="ad-container">
					<img src={require("./images/726446.svg")} /><br/>
					<p class="ad-title">آسان</p>
					<p class="ad-detail">به سادگی صاحب خانه شوید</p>
				</div>
				<div id="second-ad" class="ad-container">
					<img src={require("./images/726488.svg")} /><br/>
					<p class="ad-title">مطمئن</p>
					<p class="ad-detail">با خیال راحت به دنبال خانه بگردید</p>
				</div>
				<div id="third-ad" class="ad-container">
					<img src={require("./images/726499.svg")} /><br/>
					<p class="ad-title">گسترده</p>
					<p class="ad-detail">در منطقه مورد علاقه خود صاحب خانه شوید</p>
				</div>
			</div>
		</section>
	);
}

export default Advertisements;