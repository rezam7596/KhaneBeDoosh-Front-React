import React from 'react';
import './WhyKhaneBeDoosh.css'

function WhyKhaneBeDoosh(props) {
	return (
		<section id="why-khaneBeDoosh" className="row">
			<div id="why-khaneBeDoosh-title" className="col-xs-12">
				<span>چرا خانه به دوش؟</span>
			</div>
			<div className="col-xs-12">
				<div id="why-khaneBeDoosh-content" className="col-xs-12 col-sm-7 bsrtl">
					<p><i className="fa fa-check-circle" aria-hidden="true"></i>
						اطلاعات کامل و صحیح از املاک قابل معامله
					</p>
					<p><i className="fa fa-check-circle" aria-hidden="true"></i>
						بدون محدودیت، ۲۴ ساعته و در تمام ایام هفته
					</p>
					<p><i className="fa fa-check-circle" aria-hidden="true"></i>
						جستجوی هوشمند ملک، صرفه‌جویی در زمان
					</p>
					<p><i className="fa fa-check-circle" aria-hidden="true"></i>
						تنوع در املاک، افزایش قدرت انتخاب مشتریان
					</p>
					<p><i className="fa fa-check-circle" aria-hidden="true"></i>
						بانکی جامع از اطلاعات هزاران آگهی به‌روز
					</p>
					<p><i className="fa fa-check-circle" aria-hidden="true"></i>
						دستیابی به نتیجه مطلوب در کمترین زمان ممکن
					</p>
					<p><i className="fa fa-check-circle" aria-hidden="true"></i>
						همکاری با مشاوران متخصص در حوزه املاک
					</p>
				</div>
				<div id="why-khaneBeDoosh-pic" className="col-xs-12 col-sm-5 bsrtl">
					<img src={require("./images/why-khanebedoosh.png")} />
				</div>
			</div>
		</section>
	);
}

export default WhyKhaneBeDoosh;