import React from 'react';
import BackgroundSlideshow from '../components/background-slideshow/BackgroundSlideshow';
import MainPageHeader from '../components/header/main-page-header/MainPageHeader';
import BigLogo from "../components/BigLogo";
import SearchHouseForm from "./SearchHouseForm";
import AddHouseButton from "../components/AddHouseButton";
import Advertisements from "../components/Advertisements";
import WhyKhaneBeDoosh from "../components/WhyKhaneBeDoosh";
import Footer from "../components/Footer";
import IncreaseCreditModal from "../components/IncreaseCreditModal";
import AddNewHouseModal from "../components/AddNewHouseModal";

class MainPage extends React.Component {
	render() {
		return (
			<div>
				<BackgroundSlideshow/>
				<MainPageHeader/>
				<IncreaseCreditModal/>
				<AddNewHouseModal/>
				<div id="wrapper" className="col-xs-12 col-sm-8 col-sm-offset-2">
					<BigLogo/>
					<SearchHouseForm set_search_result={this.props.set_search_result} set_loading={this.props.set_loading}/>
					<AddHouseButton/>
					<Advertisements/>
					<WhyKhaneBeDoosh/>
				</div>
				<Footer/>
			</div>
		);
	}
}

export default MainPage;