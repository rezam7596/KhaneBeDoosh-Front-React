import React from 'react';

function SearchHouse (buildingType, maximumPrice, minimumArea, dealType, setLoading) {
    setLoading(true);
    return fetch('/api/SearchHouses?' +
        'minimum_area=' + minimumArea +
        '&building_type=' + buildingType +
        '&deal_type=' + dealType +
        '&maximum_price=' + maximumPrice
    ).then(response => {
        if (response.status === 200)
            return response.json();
        else
            throw response;
    }).catch(response => {
        alert("request to server (/ajaxSearchHouses) failed. status:" + response.status);
        return {success: "NotOk", response: response}
    });
}

export default SearchHouse;